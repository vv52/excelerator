"""
Excelerator searches for a string in every cell of all excel files in a
directory and returns the filename of the workbook and the name of the
sheet for any matches

Dependencies:
- pip install pandas=2.0.1
- pip install openpyxl=3.1.2
- pip install xlrd=2.0.1
"""

# TODO: Clean up and further modularize code
# TODO: GUI?

import pandas
import os, sys
import csv

filetypes = (".xlsx",".xls")

paths = []
workbooks = []
sheets = {}
results = []


def searchAsset(search):
    # Iterate over sheets
    print("Searching for " + search + "...")
    success = False
    search_results = []
    for name, sheet in sheets.items():

        # Search in sheet for string
        result = sheet.isin((search,))

        # Filter results on boolean true to exclude sheets that had no matches
        filtered_result = result[(result == True).any(axis=1)]

        # If a match was found in the sheet, print the filename and sheetname
        if not filtered_result.empty:
            success = True
            search_results.append(name)
            print(name)

    # If no results were found, inform the user
    if not success:
        print("No results found")
    print("")
    return search_results


def dumpCSV(results):
    while True:
        csv_name = input("Input filename: ")
        if csv_name.endswith(".csv"):
            print("\nWriting CSV...")
            with open(csv_name, mode='w') as results_csv:
                results_writer = csv.writer(results_csv, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                results_writer.writerow(("Asset", "Sheets"))
                for row in results:
                    results_writer.writerow(row)
            print("CSV written!")
            break
        else:
            print("Error! File must be *.csv")


print("Scanning directory...")

# Get paths to Excel files in current directory
for path in os.listdir():
    if path.endswith(filetypes):
        paths.append(path)

# Exit if no workbooks in valid formats are found in the current directory
if not paths:
    print("Error! No workbooks in directory")
    exit()

print("Importing workbooks...")

# Import Excel workbooks
for path in paths:
    workbooks.append(pandas.ExcelFile(path))

# Get each sheet of workbooks and store them in a dict with "file/sheet" format
for i, workbook in enumerate(workbooks):
    for sheet in workbook.sheet_names:
        sheet_path = paths[i] + "/" + sheet
        # Read in sheet and convert all strings in sheet to uppercase
        sheets[sheet_path] = pandas.read_excel(workbook, sheet).applymap(lambda s: s.upper() if type(s) == str else s)

print("Workbooks loaded!\n")

# Check if path(s) to csv(s) with searches was/were provided as a command-line argument
if len(sys.argv) > 1:
    index = 1
    while index <= len(sys.argv) - 1:
        # If so, attempt to open each and read the contents into a list
        print("Opening " + sys.argv[index] + "...")
        if not sys.argv[index].endswith(".csv"):
            print("Error! Only *.csv files are accepted")
            print("If you would like to search for a list of assets, please use a *.csv\n")
        else:
            with open(sys.argv[index]) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                searches = []
                for item in csv_reader:
                    searches += item
                print("CSV loaded!\n")
                # Convert each item in search list to uppercase and perform search
                for search in searches:
                    result = [search.upper()] + searchAsset(search.upper())
                    results.append(result)
        index += 1
    dump_to_csv = input("Save results to csv? (Y/N) ")
    if dump_to_csv.upper() == "Y":
        dumpCSV(results)

# If no arguments provided
else:
    # Prompt user for value to search in sheets until exit
    search = ""
    while search != "Q":
        # Get search string and convert it to uppercase
        search_raw = input("Please enter asset numbers separated by commas (or q to quit): ").upper()
        if search_raw == "Q":
            exit()

        # Remove whitespace from search and split on commas
        searches = search_raw.strip().split(",")

        # Search for each asset successively
        for search in searches:
            result = [search.upper()] + searchAsset(search.upper())
            results.append(result)

        dump_to_csv = input("Save results to csv? (Y/N) ")
        if dump_to_csv.upper() == "Y":
            dumpCSV(results)
